package com.example.testingfirestore.CurrentInfo

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.testingfirestore.R
import com.example.testingfirestore.databinding.CurrentInfoFragmentBinding
import com.example.testingfirestore.databinding.SignUpFragmentBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class CurrentInfoFragment : Fragment() {

    private lateinit var binding: CurrentInfoFragmentBinding
    private lateinit var mAuth: FirebaseAuth
    private lateinit var dbStore: FirebaseFirestore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CurrentInfoFragmentBinding.inflate(inflater, container, false)
        dbStore = FirebaseFirestore.getInstance()
        mAuth = FirebaseAuth.getInstance()

        val serviceCentre = resources.getStringArray(R.array.service_centers)

        val serviceCentersAdapter = ArrayAdapter(
            requireContext(), R.layout.dropdown_item,
            serviceCentre
        )
        binding.etLayout.setAdapter(serviceCentersAdapter as ArrayAdapter<*>)

        chooseCountry()

        return binding.root
    }





    private fun chooseCountry(){

        binding.etLayout.setOnItemClickListener { parent, view, position, id ->

            retrieveData()

        }

    }




    private fun retrieveData() {



        binding.tvUid.text = mAuth.currentUser?.uid
        val doc = binding.etLayout.text.toString()

        dbStore.collection("countries").document(doc)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    binding.tvName.text = document.data?.get("location").toString()
                    binding.tvPassword.text = document.data?.get("location2").toString()
                    binding.tvEmail.text = document.data?.get("location3").toString()
                } else {
                    Log.d("message2", "No such document")
                }
            }

    }

}