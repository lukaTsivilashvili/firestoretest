package com.example.testingfirestore.signUp

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.testingfirestore.R
import com.example.testingfirestore.databinding.SignUpFragmentBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class SignUpFragment : Fragment() {

    private lateinit var binding: SignUpFragmentBinding
    private lateinit var mAuth:FirebaseAuth
    private lateinit var dbStore:FirebaseFirestore


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = SignUpFragmentBinding.inflate(inflater, container, false)
        mAuth = FirebaseAuth.getInstance()
        dbStore = FirebaseFirestore.getInstance()
        setListeners()
        return binding.root
    }

    private fun setListeners(){

        binding.btnSignUp.setOnClickListener {
            signUp()
        }

        binding.btnLogIn.setOnClickListener {
            logIn()
        }

        binding.btnSignOut.setOnClickListener {

            mAuth.signOut()

            if (mAuth.currentUser?.uid == null) {
                Toast.makeText(requireContext(), "Signed Out", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Error Signing Out", Toast.LENGTH_SHORT).show()
            }

        }

        binding.btnInfoFragment.setOnClickListener {
            findNavController().navigate(R.id.action_signUpFragment_to_currentInfoFragment)
        }

    }


    private fun signUp(){

        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        val name = binding.etName.text.toString()

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful){
                Toast.makeText(requireContext(), "Signed Up", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()

            }
        }

        saveFirestore(email, password, name)

    }

    private fun logIn(){

        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()
        val name = binding.etName.text.toString()

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Toast.makeText(requireContext(), "Logged In", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun saveFirestore(email:String, password:String, name:String){


        val user:MutableMap<String,String> = mutableMapOf()
        user["email"] = email
        user["password"] = password
        user["name"] = name

        dbStore.collection("users")
            .add(user)
            .addOnSuccessListener {
                Toast.makeText(requireContext(), "added successfully", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{
                Toast.makeText(requireContext(), "failed to add", Toast.LENGTH_SHORT).show()
            }


    }

}